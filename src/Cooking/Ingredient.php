<?php


namespace App\Cooking;


class Ingredient
{
  public $name;
  public $calories;
  public $vegetale;
  public $raw;

  public function __construct(string $name, int $calorie, bool $vegetale, bool $raw)
  {
    $this->name = $name;
    $this->calorie = $calorie;
    $this->vegetale = $vegetale;
    $this->raw = $raw;
  }

  public function cook() : void
  {
    if ($this->raw) {
      $this->calorie += 10;
      $this->raw = false;
    }
  }
}