<?php

namespace App\Cooking;

class Dish extends Ingredient
{
  private $ingredients;

  public function __construct() {
    parent::__construct("",0,true,true);
    $this->ingredients = [];
  }

  public function addIngredient(Ingredient $paramIngredient): void {
    $this->ingredients[] = $paramIngredient;
    $this->calories = $paramIngredient->calories;
    if (!$paramIngredient->vegetale) {
      $this->vegetale = false;
    }
  }
}